from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.forms import TodosForm


def Todo_List(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_objects": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodosForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodosForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


# Create your views here.

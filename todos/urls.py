from django.urls import path
from todos.views import Todo_List
from todos.views import todo_list_detail
from todos.views import todo_list_create

urlpatterns = [
    path("", Todo_List, name="todo_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
]
